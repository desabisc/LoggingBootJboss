package mx.com.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class Service {
	
	/**
	 * http://localhost:8080/LoggingBootJBoss-1/
	 * 
	 * */
	@GetMapping("/")
	public String index() {
		for(int i=0; i<=10; i++) {
			log.info("Mensaje "+i);
		}
		return "logging test";
	}
}