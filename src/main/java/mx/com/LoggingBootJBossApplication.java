package mx.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoggingBootJBossApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoggingBootJBossApplication.class, args);
	}

}
